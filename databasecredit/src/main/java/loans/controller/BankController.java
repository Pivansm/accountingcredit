package loans.controller;

import loans.domain.entity.Bank;
import loans.repository.BankRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Map;
import java.util.Optional;

/**
 * Контроллер для Bank
 * @author Ivan
 */
@Slf4j
@Controller
public class BankController {

    private final BankRepository bankRepository;

    @Autowired
    public BankController(BankRepository bankRepository) {
        this.bankRepository = bankRepository;
    }

    @GetMapping("/banksList")
    public String allBank(Model model, @PageableDefault(sort = {"id"}, direction = Sort.Direction.ASC, size = 2) Pageable pageable) {
        log.info("====banks");
        //Pageable pageable = PageRequest.of(0, 10);
        Page<Bank> pageBank = bankRepository.findAll(pageable);

        int pagesCount = pageBank.getTotalPages();
        int[] pages = new int[pagesCount];
        for(int i=0; i<pagesCount; i++) pages[i] = i;

        //количество страниц
        model.addAttribute("pages", pages);
        //текущая страница
        model.addAttribute("pageCourante", pageBank.getNumber());
        //Сам список с банками разбитый на страницы.
        model.addAttribute("banks", pageBank);
        return "banks/listBanks";
    }

    @GetMapping(value = "/banks.html", params = "id")
    private String get(Long id, Model model) {
        System.out.println("id:=" + id);
        Optional<Bank> b = bankRepository.findById(id);
        bankRepository.findById(id)
                .ifPresent(bank -> model.addAttribute("bank", bank));
        System.out.println("bank:=" + b);
        return "banks/detailsBank";
    }

    @PostMapping("/banks.html")
    public String processBank(Bank bank) {
        log.info("Bank submitted" + bank);
        bankRepository.save(bank);
        return "redirect:/banks.html";
    }

    @PostMapping("bank")
    public String processCredit(Bank bk, Model model) {
        log.info("Find bank:=" + bk);
        Optional<Bank> bank = Optional.empty();
        model.addAttribute("bank", bank);
        return "banks/detailsBank";
    }

    @PostMapping(value = "deleteBank", params = "id")
    public String deleteCredit(Long id) {
        log.info("Delete bank id:=" + id);
        Optional<Bank> bank = bankRepository.findById(id);
        System.out.println("bank:=" + bank);
        bankRepository.deleteById(id);
        return "redirect:/banks.html";
    }

    @PostMapping("filter")
    public String filter(@RequestParam String filter, Model model) {
        Iterable<Bank> banks;
        if(filter != null && !filter.isEmpty()) {
            banks = bankRepository.findByNameBank(filter);
        } else {
            banks = bankRepository.findAll();
        }
        model.addAttribute("banks", banks);
        return "banks/listBanks";
    }


}
