package loans.repository;

import loans.domain.entity.Bank;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface BankRepository extends CrudRepository<Bank, Long> {

    List<Bank> findByNameBank(String nameBank);
    //С разбивкой на страницы
    Page<Bank> findAll(Pageable pageable);
}
